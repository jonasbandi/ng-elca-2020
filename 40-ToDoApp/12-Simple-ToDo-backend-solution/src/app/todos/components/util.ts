export interface ServerError {
  message: string | undefined;
}

export function handleServerError(error: ServerError): void {
  const errMsg = error.message || 'Error calling server';
  console.error(errMsg);
  alert('Error: Calling the server failed!');
  window.location.reload();
}
