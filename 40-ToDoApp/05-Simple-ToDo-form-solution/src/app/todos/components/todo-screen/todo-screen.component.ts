import { Component, OnInit } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { ToDoService } from '../../model/todo.service';

@Component({
  templateUrl: './todo-screen.component.html',
})
export class TodoScreenComponent implements OnInit {
  todos: ToDo[] = [];

  constructor(private todoService: ToDoService) {}

  ngOnInit(): void {
    this.loadToDos();
  }

  addToDo(todo: ToDo): void {
    this.todos.push(todo);
    this.todoService.saveToDos(this.todos);
  }

  completeToDo(todo: ToDo): void {
    todo.completed = true;
    this.todoService.saveToDos(this.todos);
    this.loadToDos();
  }

  private loadToDos(): void {
    const todoContainer = this.todoService.loadToDos();
    this.todos = todoContainer.todos;
  }
}
