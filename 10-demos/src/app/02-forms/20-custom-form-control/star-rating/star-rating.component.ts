import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

type ChangeHandlerFn = (stars: number) => void;
type TouchHandlerFn = () => void;
interface Rating {
  stars: number;
  text: string;
}

@Component({
  selector: 'aw-star-rating',
  template: `
    <i>{{ displayText }}</i>
    <div class="stars" [ngClass]="{ disabled: disabled }">
      <ng-container *ngFor="let star of ratings">
        <svg
          title="{{ star.text }}"
          height="25"
          width="23"
          class="star rating"
          [ngClass]="{ selected: star.stars <= ratingValue }"
          (mouseover)="displayText = !disabled ? star.text : ''"
          (mouseout)="displayText = ratingText ? ratingText : ''"
          (click)="setRating(star)"
        >
          <polygon
            points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78"
            style="fill-rule:nonzero;"
          />
        </svg>
      </ng-container>
    </div>
  `,
  styleUrls: ['./star-rating.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StarRatingComponent),
      multi: true
    }
  ]
})
export class StarRatingComponent implements ControlValueAccessor {
  constructor() {}

  public ratings = [
    {
      stars: 1,
      text: 'disappointment'
    },
    {
      stars: 2,
      text: 'meh'
    },
    {
      stars: 3,
      text: 'it is ok'
    },
    {
      stars: 4,
      text: 'I like it'
    },
    {
      stars: 5,
      text: 'Super Great!'
    }
  ];
  public disabled = false;
  public displayText = '';
  public ratingText = '';
  public ratingValue = 3;

  onChanged: ChangeHandlerFn = () => {};
  onTouched: TouchHandlerFn = () => {};

  writeValue(val: number): void {
    this.ratingValue = val;
  }

  registerOnChange(fn: ChangeHandlerFn): void {
    this.onChanged = fn;
  }

  registerOnTouched(fn: TouchHandlerFn): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  setRating(star: Rating): void {
    if (!this.disabled) {
      this.ratingValue = star.stars;
      this.ratingText = star.text;
      this.onChanged(star.stars);
      this.onTouched();
    }
  }
}
