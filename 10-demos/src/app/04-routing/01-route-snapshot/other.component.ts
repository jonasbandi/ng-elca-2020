import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'aw-routed',
  template: `
    <h1>Other Component</h1>
    <h1>Current Detail: {{ detailNo }}</h1>

    <a [routerLink]="['../../route-snapshot', (detailNo ? detailNo : 0) + 1]">Go to routed component ...</a>
    <br />
    <button (click)="goToNextDetail()">Go to next detail ...</button>
  `
})
export class OtherComponent implements OnInit {
  detailNo: number | undefined;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.detailNo = parseInt(this.route.snapshot.paramMap.get('detailNumber') ?? '0', 10);
  }

  goToNextDetail(): void {
    const nextNumber = (this.detailNo ?? 0) + 1;

    // Navigate with relative link
    this.router.navigate(['../other', nextNumber], { relativeTo: this.route });
  }
}
