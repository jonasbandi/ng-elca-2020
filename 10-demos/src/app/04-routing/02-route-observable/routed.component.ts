import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'aw-routed',
  template: `
    <h1>Routed Component</h1>
    <h1>Current Detail: {{ detailNo }}</h1>

    <br />
    <button (click)="goToNextDetail()">Go to next detail ...</button>
  `
})
export class RoutedObservableComponent implements OnInit {
  detailNo: number | undefined;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.paramMap
      .pipe(map(p => parseInt(p.get('detailNumber') ?? '0', 10)))
      .subscribe(n => (this.detailNo = n));
  }

  goToNextDetail(): void {
    const nextNumber = (this.detailNo ?? 0) + 1;

    // Navigate with relative link
    this.router.navigate(['..', nextNumber], { relativeTo: this.route });
  }
}
